from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR


class RegressorManager:

    @staticmethod
    def get_regressor(regressor_type: str, params: dict):
        match regressor_type:
            case "linear_regression":
                return LinearRegression(**params)
            case "random_forest":
                return RandomForestRegressor(**params)
            case "svr":
                return SVR(**params)
            case _:
                raise ValueError(f"Regressor type {regressor_type} not recognized")

    @staticmethod
    def get_params_choices(trial, regressor_type: str) -> dict:
        match regressor_type:
            case "linear_regression":
                return {}
            case "random_forest":
                return {"n_estimators": trial.suggest_int("n_estimators", 5, 50),
                        "max_depth": trial.suggest_int("max_depth", 3, 10),
                        "max_features": trial.suggest_int("max_features", 2, 10)}
            case "svr":
                return {"kernel": trial.suggest_categorical("kernel", ["linear", "poly", "rbf"]),
                        "C": trial.suggest_float("C", 0.1, 10),
                        "epsilon": trial.suggest_float("epsilon", 0.1, 0.5)}
            case _:
                raise ValueError(f"Regressor type {regressor_type} not recognized")
