from typing import Tuple

import numpy as np
import requests
import tarfile
from io import BytesIO
import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit


class DataReader:
    def __init__(self, data_url: str, data_folder: str = "./data"):
        self.data_url = data_url
        self.data_folder = data_folder

    def load_and_save_data(self):
        response = requests.get(self.data_url)

        tar_bytes = BytesIO(response.content)

        # Extract tar file
        with tarfile.open(fileobj=tar_bytes, mode="r:gz") as tar:
            tar.extractall(path=self.data_folder)

    def get_data(self):
        try:
            return pd.read_csv(f"{self.data_folder}/housing.csv")
        except FileNotFoundError:
            self.load_and_save_data()
        return pd.read_csv(f"{self.data_folder}/housing.csv")


class PrepareData:

    @staticmethod
    def prepare_train_test_data(data: pd.DataFrame, target: str) -> Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]:
        """
        Split the data into train and test sets
        :param data: pandas dataframe to split
        :param target: target column name
        :return: X_train, X_test, y_train, y_test
        """

        data["income_cat"] = pd.cut(data["median_income"],
                                    bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
                                    labels=[1, 2, 3, 4, 5])

        split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
        for train_index, test_index in split.split(data, data["income_cat"]):
            strat_train_set = data.loc[train_index]
            strat_test_set = data.loc[test_index]

        for set_ in (strat_train_set, strat_test_set):
            set_.drop("income_cat", axis=1, inplace=True)

        X_train = strat_train_set.drop(target, axis=1)
        X_test = strat_test_set.drop(target, axis=1)
        y_train = strat_train_set[target].copy()
        y_test = strat_test_set[target].copy()

        return X_train, X_test, y_train, y_test
