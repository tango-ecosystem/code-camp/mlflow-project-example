import mlflow
import numpy as np
import optuna
from sklearn.metrics import root_mean_squared_error
from sklearn.model_selection import KFold

from regressor_manager import RegressorManager
from model import HousingModel


def tune(X_train, y_train, n_trials, n_splits, regressor_type: str) -> dict:

    def objective(trial):
        with mlflow.start_run(nested=True):

            # K-Fold Cross Validation
            splits = KFold(n_splits=n_splits, shuffle=True, random_state=42)

            params = RegressorManager.get_params_choices(trial, regressor_type=regressor_type)
            model = HousingModel(params=params, regressor_type=regressor_type)

            scores = []
            for train_index, test_index in splits.split(X_train):
                X_train_fold = X_train.iloc[train_index]
                y_train_fold = y_train.iloc[train_index]
                X_test_fold = X_train.iloc[test_index]
                y_test_fold = y_train.iloc[test_index]

                model.fit(X_train_fold, y_train_fold)
                predictions = model.predict(context=None, model_input=X_test_fold)

                # as score use the rmse
                scores.append(root_mean_squared_error(y_true=y_test_fold, y_pred=predictions))

            scores_mean = np.mean(scores)

            # log the parameters for each nested run
            mlflow.log_params(params)
            mlflow.log_param("cv_score", "root_mean_squared_error")

            mlflow.log_metric("mean_cv_score", scores_mean)

            return scores_mean

    study = optuna.create_study(direction="minimize")

    study.optimize(objective, n_trials=n_trials)
    return study.best_trial.params
