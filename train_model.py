import mlflow
import pandas as pd
from mlflow.models import infer_signature

from model import HousingModel


def train(X_train: pd.DataFrame, y_train: pd.Series, params: dict, regressor_type: str, artifact_path: str, registered_model_name: str):

    model = HousingModel(params=params, regressor_type=regressor_type)

    model.fit(X_train, y_train)

    # define an input example
    input_example = X_train.head(5)
    signature = infer_signature(input_example, model.predict(context=None, model_input=input_example))

    # log the model and register it to the model registry
    ml_model = mlflow.pyfunc.log_model(artifact_path=artifact_path,
                                       python_model=model,
                                       input_example=input_example,
                                       signature=signature,
                                       registered_model_name=registered_model_name)

    # log params to mlflow run
    mlflow.log_params(params)

    return ml_model
