import mlflow
import pandas as pd
import click


from prepare_data import DataReader, PrepareData
from train_model import train
from tune_model import tune


@click.command()
@click.option("--data_url", default="https://raw.githubusercontent.com/ageron/handson-ml2/master/datasets/housing/housing.tgz", help="URL to the data")
@click.option("--data_folder", default="./data", help="Folder to save the data")
@click.option("--target_column", default="median_house_value", help="Target column name")
@click.option("--regressors", default="random_forest,linear_regression,svr", help="Regressors to use")
@click.option("--n_trials", default=10, help="Number of trials for hyperparameter tuning")
@click.option("--n_splits", default=5, help="Number of splits for cross validation")
def pipeline(data_url: str, data_folder: str, target_column: str, regressors: str, n_trials: int, n_splits: int) -> None:

    regressors = regressors.split(",")

    data_reader = DataReader(data_url=data_url,
                             data_folder=data_folder)

    data = data_reader.get_data()

    # train test split
    X_train, X_test, y_train, y_test = PrepareData.prepare_train_test_data(data, target=target_column)

    client = mlflow.tracking.MlflowClient()

    rmse_scores = {}

    for regressor in regressors:

        with mlflow.start_run():

            # tune model with optuna and get the best params
            best_params = tune(X_train, y_train, n_trials=n_trials, n_splits=n_splits, regressor_type=regressor)

            # train model
            saved_model = train(X_train, y_train, best_params, regressor, artifact_path="housing_model", registered_model_name="housing_model")

            model_uri = saved_model.model_uri

            # evaluate the model on the test set and log the results
            mlflow.evaluate(
                model_uri,
                pd.concat([X_test, y_test], axis=1),
                targets=target_column,
                model_type="regressor",
                evaluators=["default"]
            )

            model_version = client.get_registered_model("housing_model").latest_versions[0].version
            rmse_scores[model_version] = client.get_run(saved_model.run_id).data.metrics["root_mean_squared_error"]

    best_model_version = min(rmse_scores, key=rmse_scores.get)
    client.set_registered_model_alias("housing_model", "Challenger", best_model_version)


if __name__ == "__main__":
    pipeline()
