# MLFlow Project Example

This is an example on how to transform a notebook into a MLFlow project. You can find the referring notebook [here](https://github.com/ageron/handson-ml2/blob/master/02_end_to_end_machine_learning_project.ipynb).

The example is a simple regression model that predicts the median house value for California districts.

## How to run the project

To run the project, you need to have the requirements listed in `requirements-dev.txt` installed. Then run the following command:

```bash
mlflow run .
```

If you want to run the project with the local environment, you need to have the requirements listed in `requirements.txt` installed. Then run the following command:

```bash
mlflow run . --env-manager=local
```

If you want to save the MLflow run on a specific experiment (default is `Default`), you can set the experiment name as environment variable:

```
MLFLOW_EXPERIMENT_NAME="experiment_name"
```

You can also specify the tracking uri (default is `http://localhost:5000`):

```
MLFLOW_TRACKING_URI="http://localhost:5000"
MLFLOW_TRACKING_USERNAME = "username"
MLFLOW_TRACKING_PASSWORD="password"
```

## Project structure

The project structure is as follows:

- `MLproject` is the MLproject file that defines the project. It contains a single entry point called `main`, that executes the main.py script. 
- `main.py` is the main script that trains the model, logs the metrics and artifacts, and registers the model in the Model Registry.
- `prepare_data.py` is a script that prepares the data for training, loading the dataset from github, saving the dataset `housing.csv` in the `data` folder, and splitting the dataset into train and test sets.
- `tune_model.py` contains the logic for hyperparameter tuning using Optuna.
- `train_model.py` contains the logic for training the model with the best parameters found.
- `python_env.yaml` contains the python version and the dependencies required to run the project within a virtual environment build with virtualenv.
- `model.py` contains the classes that define the model that will be trained and loaded as pyfunc on MLflow tracking.
- `regressor_manager.py` contains the class that manages the regressor types supported by the project.


The parameters for the main entry point are:
- data_url: url of the dataset to download
- regressors: regressors to train the model. Supported values are: random_forest, linear_regression, svr
- target_column: name of the target column in the dataset
- data_folder: folder to save the dataset
- n_trials: number of trials for hyperparameter tuning
- n_splits: number of splits for cross-validation

### Regressors
Supported regressors are:
- Random Forest
- Linear Regression
- Support Vector Regressor

All regressors share the same preprocessing pipeline, implemented in `model.py`.

The class `RegressorManager` in `regressor_manager.py` is responsible for managing the regressors types supported, as
well as the hyperparameters search space for each regressor.
