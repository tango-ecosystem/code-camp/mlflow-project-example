import numpy as np
from mlflow.pyfunc import PythonModel
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin

from regressor_manager import RegressorManager


class HousingModel(PythonModel):

    def __init__(self, params: dict, regressor_type: str):
        # params is a dictionary of hyperparameters for the regressor. You can add also preprocessing parameters
        self.params = params
        self.regressor_type = regressor_type
        self.full_pipeline = None
        self.regressor = None

    def fit(self, X, y=None):

        num_attribs = list(X.select_dtypes(include=[np.number]))
        cat_attribs = list(X.select_dtypes(include=[object]))

        col_names = "total_rooms", "total_bedrooms", "population", "households"
        rooms_ix, bedrooms_ix, population_ix, households_ix = [X.columns.get_loc(c) for c in col_names]

        num_pipeline = Pipeline([
            ('imputer', SimpleImputer(strategy="median")),
            ('attribs_adder', CombinedAttributesAdder(rooms_ix, bedrooms_ix, population_ix, households_ix)),
            ('std_scaler', StandardScaler())
        ])

        self.full_pipeline = ColumnTransformer([
            ("num", num_pipeline, num_attribs),
            ("cat", OneHotEncoder(handle_unknown='ignore'), cat_attribs),
        ])

        housing_prepared = self.full_pipeline.fit_transform(X)

        self.regressor = RegressorManager.get_regressor(self.regressor_type, params=self.params)
        self.regressor.fit(housing_prepared, y)

        return self

    def predict(self, context, model_input, params=None):
        preprocessed_input = self.full_pipeline.transform(model_input)
        return self.regressor.predict(preprocessed_input)


class CombinedAttributesAdder(BaseEstimator, TransformerMixin):

    def __init__(self, rooms_ix, households_ix, population_ix, bedrooms_ix, add_bedrooms_per_room=True):
        self.rooms_ix = rooms_ix
        self.households_ix = households_ix
        self.population_ix = population_ix
        self.bedrooms_ix = bedrooms_ix
        self.add_bedrooms_per_room = add_bedrooms_per_room

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        rooms_per_household = X[:, self.rooms_ix] / X[:, self.households_ix]
        population_per_household = X[:, self.population_ix] / X[:, self.households_ix]

        if self.add_bedrooms_per_room:
            bedrooms_per_room = X[:, self.bedrooms_ix] / X[:, self.rooms_ix]
            return np.c_[X, rooms_per_household, population_per_household,
                         bedrooms_per_room]
        else:
            return np.c_[X, rooms_per_household, population_per_household]
